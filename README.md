# Mastodon-Installation, mit Ansible-Playbook

- [Installations-Anleitung - Schritt für Schritt](https://docs.joinmastodon.org/admin/prerequisites/)
- [Quellcode](https://github.com/mastodon/mastodon)
- [Installation über Ansible](https://github.com/mastodon/mastodon-ansible/)

## Zwei Maschinen
Es wird eine Maschine als Mastodon-Server benötigt. Obwohl das Ansible-Skript auch ausgewählte andere Distributionen unterstützt, ist die Installation mit Ubuntu 18.04 LTS die zu bevorzugende Variante. In dieser Anleitung wird also ein Ubuntu 18.04 LTS mit dem Hostnamen `masto.lfb.local` verwendet. Hostnamen kann mit `hostnamectl masto.lfb.local` gesetzt werden. Außerdem einen Lookup über die `/etc/hosts` ermöglichen. Dort also z.B. die Zeile 

`10.1.21.122     masto.lfb.local masto`

eintragen. Die IP-Adresse ist die LAN-Schnittstelle des zukünftigen Mastodon-Servers.

Außerdem wird ein Linux-Client als Ansible-Host benötigt, in dieser Anleitung ein Debian 11 Testing (Bullseye) und als `deb11vm.lfb.local` bezeichnet.

## Vorbereitung beider Maschinen
- SSH-Pub-Key vom `deb11vm`-Client mit `ssh-copy-id` als User auf Server kopieren und zertifikatsbasiert anmelden. Ggf. vorher den Key mit `ssh-keygen` erzeugen.
- Per `su -` root werden und den Ordner `~/.ssh` des Users in das HomeDir von `root` kopieren.
- Funktionierenden Zertifikatslogin als `root` testen - muss funktionieren.
- In `/etc/ssh/sshd_config` die Zeile `PasswordAuthentication` aktivieren, den Wert auf `no` setzen und den `sshd` mit `systemctl restart sshd` neu starten.

**WICHTIG 1:** Von `deb11vm` aus muss man sich als `root` und als ein normaler User per Zertifikat an der Maschine `masto` anmelden können.

**WICHTIG 2:** Der normale User auf `masto` muss `sudo`-Berechtigung haben. Dazu ggf. den Usernamen in der Gruppe `adm` von `/etc/group` ergänzen! Testen!!

## Installation via Ansible

### Auf dem Ansible-Host durchzuführen

Die Installaton wird per Ansible-Playbook vom Linux-Client `deb11vm` aus gestartet. Deshalb dort folgende Schritte durchführen:

- Kontrollieren ob ein `python3` vorhanden ist, andernfalls installieren

```
#!/bin/bash

cd ~
apt update
apt -y install --no-install-recommends virtualenv python3-pip git
virtualenv -p /usr/bin/python3 env
source env/bin/activate
git clone https://github.com/agru/mastodon-ansible.git
cd mastodon-ansible
pip install -r requirements.txt
```

Damit sind auf dem Ansible-Host, wie von der Installationsanleitung vorgeschlagen, alle Voraussetzungen erfüllt. Nun kann über ein Ansible-Playbook von `deb11vm.lfb.local` aus die Installation von Mastodon auf `masto.lfb.local` vorgenommen werden.

**ACHTUNG:** 
- Die Namen müssen auflösbar sein (per DNS oder per `/etc/hosts`) und auf die richtigen Maschinen verweisen.
- Für einen echten Server, der im Internet erreichbar ist, benötigt man ein SSL-/TLS-Zertifikat. In diesem Fall im nachfolgenden Kommando den Parameter `--skip-tags "letsencrypt"` nicht nutzen.

Der nächste Befehl startet die Installation per Ansible, lässt dabei die Erstellung eines SSL-/TLS-Zertifikats aber aus.

```
source env/bin/activate
cd mastodon-ansible/bare
ansible-playbook playbook.yml -i masto.lfb.local, -u user --ask-become-pass \
    -e 'ansible_python_interpreter=/usr/bin/python3' --skip-tags "letsencrypt" \
	  --extra-vars="mastodon_db_password=geheim mastodon_host=masto.lfb.local"
```

Die Abarbeitung aller Tasks dauert "eine Weile". Am Ende schlägt der `TASK [web : Reload nginx]` im hier gezeigten Fall fehl, weil die *Lets Encrypt*-Zertifikate fehlen. Das regeln wir nun aber auf dem Mastodon-Server.

### Auf dem Mastodon-Server durchzuführen

#### Nginx mit selbstsigniertem Zertifikat fixen

In der Datei `/etc/nginx/sites-available/mastodon.conf` den SSL-Zertifikatsbereich umkonfigurieren:

```
  # ssl_certificate     /etc/letsencrypt/live/masto.lfb.local/fullchain.pem;
  # ssl_certificate_key /etc/letsencrypt/live/masto.lfb.local/privkey.pem;
  ssl_certificate     /etc/ssl/certs/ssl-cert-snakeoil.pem;
  ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;
```

Dann den Nginx mit `systemctl restart nginx.service` neu starten, was nun funktionieren sollte.

#### Mastodon fertig konfigurieren und starten

Im Rahmen der Installation wurde Mastodon unter einem eigenen User `mastodon` installiert. Mit diesem User muss nun noch das Mastodon-Setup durchgeführt werden.

```
root@masto:~# su - mastodon
mastodon@masto:~$ cd live
mastodon@masto:~/live$ RAILS_ENV=production bundle exec rake mastodon:setup
```

Es folgen nun diverse, für das Setup relevante Abfragen. Die nachfolgende Ausgabe zeigt diese Fragen, lässt dafür aber rein diagnostische Ausgaben vom weiteren Setup weg.

**WICHTIG:** Es wird das Passwort der PostgreSQL-Datenbank abgefragt. Das wurde beim Aufruf des Ansible-Playbooks festgelegt. Verwenden Sie als Antwort an der Stelle also das beim Playbook-Aufruf verwendete Passwort!

```
Your instance is identified by its domain name. Changing it afterward will break things.
Domain name: masto.lfb.local

Single user mode disables registrations and redirects the landing page to your public profile.
Do you want to enable single user mode? No

Are you using Docker to run Mastodon? no

PostgreSQL host: /var/run/postgresql
PostgreSQL port: 5432
Name of PostgreSQL database: mastodon_production
Name of PostgreSQL user: mastodon
Password of PostgreSQL user: geheim
Database configuration works! 🎆

Redis host: localhost
Redis port: 6379
Redis password: 
Redis configuration works! 🎆

Do you want to store uploaded files on the cloud? No

Do you want to send e-mails from localhost? No
SMTP server: smtp.mailgun.org
SMTP port: 587
SMTP username: 
SMTP password: 
SMTP authentication: plain
SMTP OpenSSL verify mode: none
E-mail address to send e-mails "from": Mastodon <notifications@masto.lfb.local>
Send a test e-mail with this configuration right now? no

This configuration will be written to .env.production
Save configuration? Yes
...
All done! You can now power on the Mastodon server 🐘

Do you want to create an admin user straight away? Yes
Username: admin
E-mail: andreas.grupp@gmx.de
You can login with the password: 99413c25d93f07618b7b90426ec0a684
You can change your password once you login.
```

**Beachten:** Die Mailadresse und das Passwort sichern, damit erfolgt der Login am Server.

Sollte es notwendig sein das Setup noch einmal zu starten, z.B. wegen einer Fehleingabe, muss man zum Überschreiben der schon existierenden Datenbank vorher eine Umgebungsvariable setzen:

`export DISABLE_DATABASE_ENVIRONMENT_CHECK=1`

Spätestens jetzt kann man den Mastodon-Server selbst erstmals starten:

```
root@masto:/etc/systemd/system# systemctl daemon-reload 
root@masto:/etc/systemd/system# systemctl enable --now mastodon-web mastodon-sidekiq mastodon-streaming
```

Nach kurzer Wartezeit sollte im Netz nun [https://masto.lfb.local](https://masto.lfb.local) abrufbar sein. Falls es zu schnell versucht wird, kann evtl. noch ein Mastodon mit einer Fehlermeldung angezeigt werden. Nach ein "paar" Sekunden noch mal neu laden ... irgendwann sollte das weg sein und dafür die Startseite des neuen Mastodon-Servers verfügbar sein.

Die initialen Login-Daten des Admin-Accounts stehen am Ende des Setup-Prozesses


